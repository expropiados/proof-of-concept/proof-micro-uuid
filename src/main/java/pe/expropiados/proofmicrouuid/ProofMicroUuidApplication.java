package pe.expropiados.proofmicrouuid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ProofMicroUuidApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProofMicroUuidApplication.class, args);
    }

}
