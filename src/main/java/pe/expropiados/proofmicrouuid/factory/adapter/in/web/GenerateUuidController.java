package pe.expropiados.proofmicrouuid.factory.adapter.in.web;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.expropiados.proofmicrouuid.common.hexagonal.WebAdapter;
import pe.expropiados.proofmicrouuid.factory.application.port.in.GenerateUuidUseCase;

import java.util.UUID;

@WebAdapter
@RestController
@RequiredArgsConstructor
public class GenerateUuidController {

    private final GenerateUuidUseCase generateUuidUseCase;

    @GetMapping("/uuids-factory")
    @ApiOperation(value = "Get a random uuid")
    public UUID getUser() {
        return generateUuidUseCase.generateUuid();
    }
}
