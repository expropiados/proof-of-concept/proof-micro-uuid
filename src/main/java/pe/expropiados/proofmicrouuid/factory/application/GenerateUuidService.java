package pe.expropiados.proofmicrouuid.factory.application;

import lombok.RequiredArgsConstructor;
import pe.expropiados.proofmicrouuid.common.hexagonal.UseCase;
import pe.expropiados.proofmicrouuid.factory.application.port.in.GenerateUuidUseCase;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class GenerateUuidService implements GenerateUuidUseCase {

    @Override
    public UUID generateUuid() {
        return UUID.randomUUID();
    }
}
