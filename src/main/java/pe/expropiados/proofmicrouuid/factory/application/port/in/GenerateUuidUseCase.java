package pe.expropiados.proofmicrouuid.factory.application.port.in;

import java.util.UUID;

public interface GenerateUuidUseCase {
    UUID generateUuid();
}
